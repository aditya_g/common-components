const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const path = require('path');

module.exports={
    mode: 'development',
    entry: {
        'app': './index.js',
    },
    output: {
        filename: "[name].js",
        path: path.resolve(__dirname, "dist"),
        libraryExport: 'default'
    },
    resolve: {
        alias: {
            components: path.resolve(__dirname, 'packages')
        },
        extensions: ['.js', '.jsx'],
    },
    module: {
        rules: [{
            use: 'babel-loader',
            test: /\.(js|jsx)$/,
            exclude: /node_modules/,
        },
        // {
        //     test: /\.css$/i,
        //     use: ["style-loader", "css-loader"],
        //   },
            {
              test: /\.css$/i,
              use: [MiniCssExtractPlugin.loader, "css-loader"],
            },
    ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'My App',
            template: './index.html',
            filename: './index.html'
        }),
        new MiniCssExtractPlugin()
    ],

}