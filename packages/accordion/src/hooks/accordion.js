const { useState, useEffect, useMemo } = require("react")
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export const useAccordion = (icon,
    CloseIconHandler,
    headerComponent,
    bodyComponent,
    bodyExpanded,
    headerClassname,
    bodyClassname,
    iconClassname) => {
    
    const [IsExpanded, setExpanded] = useState(bodyExpanded);

    useEffect(() => { setExpanded(IsExpanded) },[IsExpanded]);

    const getHeader = () => <div className={headerClassname}>
        {headerComponent}
        {accordionController()}
    </div>

    const getBody = () => <div className={bodyClassname}> { IsExpanded && bodyComponent}</div>

    const accordionController = () => <div className={iconClassname}>
         X
    </div>
    
    return [
        IsExpanded,
        setExpanded,
        getHeader,
        getBody
    ]
}