import { useAccordion } from "../hooks"
import { faCoffee } from '@fortawesome/free-solid-svg-icons'
import './accordion.css';

export const Accordion = ({
    icon= faCoffee,
    CloseIconHandler,
    iconClassname=iconClassname,
    headerClassname=headerClassname,
    headerComponent=<div>i am header</div>,
    bodyComponent=<div> i am body</div>,
    bodyClassname=bodyClassname,
    bodyExpanded
}) => {
    const [
        IsExpanded,
        setExpanded, 
        getHeader, 
        getBody
    ] = useAccordion(icon,
        CloseIconHandler,
        headerComponent,
        bodyComponent,
        bodyExpanded,
        headerClassname,
        bodyClassname,
        iconClassname);
    return (
        <div>
            {getHeader()}
            {getBody()}
        </div>
    )
}